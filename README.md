# README #

### 目的 ###

* 介紹＆應用 Android JetPack ViewModel

### 說明 ###

* demo1:viewModel入門，數據不丟失
* demo2:LiveDate + ViewModel
* 後續開發 viewModel 裡面數據都是LiveData

### 總結 ###

* ViewModel好處：
* 管理數據
* 重建不丟失
