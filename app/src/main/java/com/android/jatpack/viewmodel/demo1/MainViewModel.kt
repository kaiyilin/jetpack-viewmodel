package com.android.jatpack.viewmodel.demo1

import androidx.lifecycle.ViewModel

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/15
 * Usage:
 *
 **************************************************************/

//對數據進行管理
class MainViewModel : ViewModel() {

    //橫直屏切換 數據不會消失
    var number = 0

}