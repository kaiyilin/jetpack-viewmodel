package com.android.jatpack.viewmodel.demo1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.android.jatpack.viewmodel.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel
    //沒有viewModel 數據會丟失（切換橫直屏幕會重新執行生命週期）
    var number = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        //切換橫直屏幕會重新執行生命週期
        result.text = number.toString()
//        result.text = mainViewModel.number.toString()

        btn1.setOnClickListener {
//            mainViewModel.number += 1
//            result.text = mainViewModel.number.toString()
            number += 1
            result.text = number.toString()
        }

        btn2.setOnClickListener {
//            mainViewModel.number += 10
//            result.text = mainViewModel.number.toString()
            number += 10
            result.text = number.toString()
        }

    }
}