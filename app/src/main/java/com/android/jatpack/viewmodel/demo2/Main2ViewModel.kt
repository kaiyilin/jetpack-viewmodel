package com.android.jatpack.viewmodel.demo2

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/17
 * Usage:
 *
 **************************************************************/
class Main2ViewModel : ViewModel() {

    var liveDataNumber = MutableLiveData<Int>().apply { postValue(0) }

    //提供給外部觸發
    fun addNumber(number: Int) {
        liveDataNumber.value = liveDataNumber.value?.plus(number)
    }

}