package com.android.jatpack.viewmodel.demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.jatpack.viewmodel.R
import kotlinx.android.synthetic.main.activity_main.btn1
import kotlinx.android.synthetic.main.activity_main.btn2
import kotlinx.android.synthetic.main.activity_main.result
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    private lateinit var mainViewModel: Main2ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        mainViewModel = ViewModelProvider(this).get(Main2ViewModel::class.java)

        //切換橫直屏幕會重新執行生命週期
        result.text = mainViewModel.liveDataNumber.value.toString()

        //通過LiveData感應去更新UI
        mainViewModel.liveDataNumber.observe(this, object : Observer<Int> {
            override fun onChanged(number: Int?) {
                //統一變更處理值
                result.text = number.toString()
            }
        })

        btn1.setOnClickListener {
            mainViewModel.addNumber(1)
        }

        btn2.setOnClickListener {
            mainViewModel.addNumber(2)
        }

        btn3.setOnClickListener {
            mainViewModel.addNumber(3)
        }

        btn4.setOnClickListener {
            mainViewModel.addNumber(4)
        }

        btn5.setOnClickListener {
            mainViewModel.addNumber(5)
        }
    }


}